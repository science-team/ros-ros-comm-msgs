Source: ros-ros-comm-msgs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>,
 Leopold Palomo-Avellaneda <leo@alaxarxa.net>,
 Timo Röhling <roehling@debian.org>,
Build-Depends:
 catkin,
 debhelper-compat (= 13),
 dh-ros,
 dh-sequence-python3,
 libroscpp-core-dev,
 libstd-msgs-dev,
 python3-setuptools,
 ros-message-generation,
 ros-std-msgs,
Standards-Version: 4.7.0
Section: libs
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/common_msgs
Vcs-Browser: https://salsa.debian.org/science-team/ros-ros-comm-msgs
Vcs-Git: https://salsa.debian.org/science-team/ros-ros-comm-msgs.git

Package: ros-rosgraph-msgs
Section: devel
Architecture: all
Multi-Arch: foreign
Depends:
 ros-std-msgs,
 ${misc:Depends},
Description: Messages relating to the Robot OS Computation Graph, definitions
 This package is part of Robot OS (ROS). It contains the definitions of
 the messages relating to the ROS Computation Graph. Most users are not
 expected to interact with the messages in this package, and it is
 strongly advised against. These messages are generally wrapped in
 higher level APIs.

Package: cl-rosgraph-msgs
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends:
 cl-std-msgs,
 ${misc:Depends},
Description: Messages relating to the Robot OS Computation Graph, LISP bindings
 This package is part of Robot OS (ROS). It contains the LISP
 interface to messages relating to the ROS Computation Graph. Most
 users are not expected to interact with messages in this package, and
 it is strongly advised against. These messages are generally wrapped
 in higher level APIs.

Package: cl-std-srvs
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Common service definitions, LISP bindings
 This package is part of Robot OS (ROS). It contains common service
 definitions.
 .
 This package contains the LISP interface to common service definitions.

Package: libros-rosgraph-msgs-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libstd-msgs-dev,
 ${misc:Depends},
Description: Messages relating to the Robot OS Computation Graph
 This package is part of Robot OS (ROS). rosgraph_msgs contains
 messages relating to the ROS Computation Graph. Most users are not
 expected to interact with the messages in this package, and it is
 strongly advised against. These messages are generally wrapped in
 higher level APIs.

Package: libstd-srvs-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libroscpp-core-dev,
 ${misc:Depends},
Description: Robot OS Common service definitions
 This package is part of Robot OS (ROS). It contains common service
 definitions.
 .
 This package contains C++ development files for common service definitions.

Package: python3-rosgraph-msgs
Section: python
Architecture: all
Depends:
 python3-genpy,
 python3-std-msgs,
 ${misc:Depends},
 ${python3:Depends},
Description: Messages relating to the Robot OS Computation Graph, Python 3 bindings
 This package is part of Robot OS (ROS). It contains the Python
 interface to messages relating to the ROS Computation Graph. Most
 users are not expected to interact with messages in this package, and
 it is strongly advised against. These messages are generally wrapped
 in higher level APIs.
 .
 This package contains the generated Python 3 package.

Package: python3-std-srvs
Section: python
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Robot OS Common service definitions, Python 3 bindings
 This package is part of Robot OS (ROS). It contains common service
 definitions.
 .
 This package contains the Python 3 interface to common service definitions.

Package: ros-std-srvs
Section: devel
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Robot OS Common service definitions, definitions
 This package is part of Robot OS (ROS). It contains common service
 definitions.
 .
 This package contains the message definitions of the common service
 definitions messages.
