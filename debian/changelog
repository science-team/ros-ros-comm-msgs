ros-ros-comm-msgs (1.11.3-4) unstable; urgency=medium

  * Wrap and sort Debian package files
  * Migrate to dh-ros
  * Bump Standards-Version to 4.7.0
  * Add myself to uploaders

 -- Timo Röhling <roehling@debian.org>  Thu, 26 Sep 2024 23:50:28 +0200

ros-ros-comm-msgs (1.11.3-3) unstable; urgency=medium

  * Drop unused python3-all dependency for cross builds
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 16 Jun 2023 20:40:04 +0200

ros-ros-comm-msgs (1.11.3-2) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 12:13:42 +0100

ros-ros-comm-msgs (1.11.3-1) unstable; urgency=medium

  * New upstream version 1.11.3
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 20:35:31 +0200

ros-ros-comm-msgs (1.11.2-10) unstable; urgency=medium

  * Drop Python 2 packages (Closes: #938394)
  * simplify d/watch
  * simplify d/rules
  * switch to debhelper-compat and debhelper 12
  * Bump policy version (no changes)
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 26 Oct 2019 08:03:40 +0200

ros-ros-comm-msgs (1.11.2-9) unstable; urgency=medium

  * Add MA hints
  * Add Python 3 packages

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 27 Jun 2018 20:16:32 +0200

ros-ros-comm-msgs (1.11.2-8) unstable; urgency=medium

  * Make libros-rosgraph-msgs-dev arch:any due to arch:any dependency
  * Bump policy and debhelper versions
  * Add MA hints
  * Update libstd-srvs-dev

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 17 Jun 2018 19:28:12 +0200

ros-ros-comm-msgs (1.11.2-7) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * Rebuild against new gencpp

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 02 Apr 2018 23:04:39 +0200

ros-ros-comm-msgs (1.11.2-6) unstable; urgency=medium

  * add Multi-Arch according to hinter
  * Update watch file
  * Update debhelper and policy versions
  * rebuild due to changes in ros-genpy

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 11 Jul 2017 06:38:44 +0200

ros-ros-comm-msgs (1.11.2-5) unstable; urgency=medium

  * rebuild due to changes in ros-genpy

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 16 Nov 2016 22:03:03 +0100

ros-ros-comm-msgs (1.11.2-4) unstable; urgency=medium

  [ Daniele E. Domenichelli ]
  * Add missing build dependency (Closes: #835576)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 29 Aug 2016 23:36:48 +0200

ros-ros-comm-msgs (1.11.2-3) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update URLs
  * Update my email address
  * Fix lintian warnings

  [ Daniele E. Domenichelli ]
  * Move message definitions in separate packages

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 20 Aug 2016 13:01:52 +0200

ros-ros-comm-msgs (1.11.2-2) unstable; urgency=medium

  * Rebuild for changes in ros-genlisp.

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 13 Jul 2016 08:09:54 +0200

ros-ros-comm-msgs (1.11.2-1) unstable; urgency=medium

  * Imported Upstream version 1.11.2
  * Adopt package description

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jul 2016 09:39:49 +0200

ros-ros-comm-msgs (1.11.1-3) unstable; urgency=medium

  * Add dependencies

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Mon, 11 Jan 2016 15:22:13 +0100

ros-ros-comm-msgs (1.11.1-2) unstable; urgency=medium

  * Convert to new catkin with multiarch
  * Adopt to {ros-}message-{generation,runtime}

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 24 Nov 2015 18:55:50 +0100

ros-ros-comm-msgs (1.11.1-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Initial release (Closes: #804922)
  * Add patch for toplevel CMakeLists.txt
  * Add dh_install files
  * Install actual header files
  * Update packing copyright
  * Update build dependencies
  * Add packing Python and LISP packaging
  * Update dependencies
  * Update rules
  * Add watch file
  * Fix copyright year

  [ Leopold Palomo-Avellaneda ]
  * Updated copyright
  * Updated Uploaders and Maintainer field
  * Increase Standards-Version to 3.9.6
  * Updated VCS fields
  * Updated Description fields to avoid lintian warnings
  * Updated Python dependencies

  [ Jochen Sprickerhof ]
  * Imported Upstream version 1.11.1

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Thu, 19 Nov 2015 19:06:25 +0000
